﻿using System;
using UnityEngine;

public class SwipeDetector : MonoBehaviour
{
    private Vector2 fingerDownPosition;
    private Vector2 fingerUpPosition;
    public int rotationkontrolsag, rotationkontrolsol , x, y;
    public float position, forward, hiz;
    public bool kaydiSag, kaydiSol, kaydiYukari, kaydiAsagi;

    [SerializeField]
    private bool detectSwipeOnlyAfterRelease = false;

    [SerializeField]
    private float minDistanceForSwipe = 5f;

    public static event Action<SwipeData> OnSwipe = delegate { };

    public void Start()
    {
        hiz = (PlayerPrefs.GetInt("level") / 50) + 10;
    }
    private void FixedUpdate()
    {
       
        forward += 0.1f;
       // transform.position= new Vector3 (8.15f - forward, 4.44f, -1.12f);
        transform.Translate(0, 0, hiz);

       
        
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                fingerUpPosition = touch.position;
                fingerDownPosition = touch.position;
            }

            if (!detectSwipeOnlyAfterRelease && touch.phase == TouchPhase.Moved)
            {
                fingerDownPosition = touch.position;
                DetectSwipe();
            }

            if (touch.phase == TouchPhase.Ended)
            {

                fingerDownPosition = touch.position;
                DetectSwipe();
                kaydiSag = false;
                kaydiSol = false;

            }
            if (kaydiSag && x<32)
            {
                transform.Translate(3.6f, 0, 0);
                x += 1;
            }
            if (kaydiSol && x>-32)
            {
                transform.Translate(-3.6f, 0, 0);
                x -= 1;

            }
            /*if (kaydiYukari && y<10)
            {
               transform.Translate(0, 3.6f, 0);
                y += 1;
            }
            if (kaydiAsagi && y>0)
            {
                transform.Translate(0, -3.6f, 0);
                y -= 1;

            }*/
        }
    }

    private void DetectSwipe()
    {
        if(fingerDownPosition.x > fingerUpPosition.x)
        {
            kaydiSag = true;
            kaydiSol = false;
        }
        if (fingerDownPosition.x < fingerUpPosition.x)
        {
            kaydiSol = true;
            kaydiSag = false;
        }
        if (fingerDownPosition.y < fingerUpPosition.y)
        {
            kaydiAsagi = true;
            kaydiYukari = false;
        }
        if (fingerDownPosition.y > fingerUpPosition.y)
        {
            kaydiYukari = true;
            kaydiAsagi = false;
        }
        if (SwipeDistanceCheckMet())
        {
            if (IsVerticalSwipe())
            {
                var direction = fingerDownPosition.y - fingerUpPosition.y > 0 ? SwipeDirection.Up : SwipeDirection.Down;
                SendSwipe(direction);
            }
            else
            {
                var direction = fingerDownPosition.x - fingerUpPosition.x > 0 ? SwipeDirection.Right : SwipeDirection.Left;
                SendSwipe(direction);
            }
            fingerUpPosition = fingerDownPosition;
        }
    }

    private bool IsVerticalSwipe()
    {
        return VerticalMovementDistance() > HorizontalMovementDistance();
    }

    private bool SwipeDistanceCheckMet()
    {
        return VerticalMovementDistance() > minDistanceForSwipe || HorizontalMovementDistance() > minDistanceForSwipe;
    }

    private float VerticalMovementDistance()
    {
        return Mathf.Abs(fingerDownPosition.y - fingerUpPosition.y);
    }

    private float HorizontalMovementDistance()
    {
        return Mathf.Abs(fingerDownPosition.x - fingerUpPosition.x);
    }

    private void SendSwipe(SwipeDirection direction)
    {
        SwipeData swipeData = new SwipeData()
        {
            Direction = direction,
            StartPosition = fingerDownPosition,
            EndPosition = fingerUpPosition
        };
        OnSwipe(swipeData);
    }
}

public struct SwipeData
{
    public Vector2 StartPosition;
    public Vector2 EndPosition;
    public SwipeDirection Direction;
}

public enum SwipeDirection
{
    Up,
    Down,
    Left,
    Right
}