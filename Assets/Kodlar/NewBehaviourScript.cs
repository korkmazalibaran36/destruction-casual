using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    public bool birmi;
    public float x, y, z;
    public GameObject BoomPrefab, SonSecilen;
    public Animator anim;
    public string adim;
    void Start()
    {
        if(GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().Roketim.name == "YokEdildi")
        {
            birmi = true;
            transform.localPosition = new Vector3(0, 0, 0);
            GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().Roketim = this.gameObject;
        }
        anim = this.GetComponent<Animator>();
        if (!birmi)
        {
            //this.name = "RPG-7 Rocket" + (GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().degerim - 1).ToString();
            x = Random.Range(-40f, 40.8f);
            z = Random.Range(-30.5f, 60f);
            this.transform.localPosition = new Vector3(x, -4, z);
        }
    }

    // Update is called once per frame
    void Update()
    {
        adim = this.name;
        if (GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().Geldi)
        {
            anim.Play("Attack");

        }
        else
        {
            anim.Play("Run");
        }
        if (!birmi)
        {
            this.transform.localPosition = new Vector3(x, y, z);
            this.transform.localEulerAngles = new Vector3(0, 0, 0);
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Engeller")
        {
            GameObject.Instantiate(BoomPrefab, this.transform.position, Quaternion.identity);
            anim.Play("Death");
            GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().degerim -= 1;
            this.name = "YokEdildi";
            GameObject.Find("Karakter").GetComponent<AudioSource>().Play();
            this.gameObject.SetActive(false);
        }
       
        if (other.gameObject.tag == "ZombieBu")
        {
            GameObject.Instantiate(BoomPrefab, this.transform.position, Quaternion.identity);
            anim.Play("Death");
            GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().degerim -= 1;
            this.name = "YokEdildi";
            GameObject.Find("Karakter").GetComponent<AudioSource>().Play();
            this.gameObject.SetActive(false);

        }
    }
    public void Olum(Collider other)
    {
        if (other.gameObject.tag == "Engeller" || other.gameObject.tag == "ZombieBu")
        {
            if (other.gameObject.tag == "Engeller")
            {
                GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().degerim -= 1;
                this.name = "YokEdildi";
                GameObject.Find("Karakter").GetComponent<AudioSource>().Play();
            }
            if (!birmi)
            {
                //GameObject.Instantiate(BoomPrefab, this.transform.position, Quaternion.identity);

                //this.gameObject.SetActive(false);
            }
            else
            {
                if (GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().degerim == 1)
                {
                  //  GameObject.Instantiate(BoomPrefab, this.transform.position, Quaternion.identity);
                    GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().degerim -= 1;
                    GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().aud.Stop() ;
                    GameObject.Find("Karakter").GetComponent<AudioSource>().Play();
                   // this.gameObject.SetActive(false);
                   
                }
                else
                {
                    for (int i = 0; i < 10000; i++)
                    {
                        if (GameObject.Find("RPG-7 Rocket" + i.ToString()) != null)
                        {
                           // GameObject.Instantiate(BoomPrefab, this.transform.position, Quaternion.identity);
                            SonSecilen =  GameObject.Find("RPG-7 Rocket" + i.ToString());
                            SonSecilen.name = "YokEdildi";
                            SonSecilen.SetActive(false);
                            GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().degerim -= 1;
                            GameObject.Find("Karakter").GetComponent<AudioSource>().Play();
                            i = 1000;
                        }

                    }
                }
            }
        }
    }
}
