using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rastgele : MonoBehaviour
{
    public int a,b;
    public bool ikili, dokunakli,degdi;
    public GameObject[] objeler;

    void Start()
    {
        if (!dokunakli)
        {
            objeler[Random.Range(0, a)].SetActive(true);
            if (ikili)
            {
                objeler[Random.Range(0, b)].SetActive(true);

            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (dokunakli)
        {
            if (degdi)
            {
                objeler[Random.Range(0, a)].SetActive(true);
                if (ikili)
                {
                    objeler[Random.Range(0, b)].SetActive(true);

                }
                degdi = false;
            }
        }
    }
}
