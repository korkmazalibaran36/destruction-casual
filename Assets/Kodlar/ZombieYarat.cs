using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZombieYarat : MonoBehaviour
{
    public int a, b,i, Degerim;
    public GameObject prefabZombie, Zombie, col1,col2, karakter, winpanel;
    public bool degildi, finish;
    public Text leveltxt;


    void Start()
    {
        col2.GetComponent<BoxCollider>().isTrigger = false;
        karakter = GameObject.Find("Karakter");
        b = Random.Range(4, a);
        if (finish)
        {
            b = PlayerPrefs.GetInt("level") + 40;
            leveltxt.text = PlayerPrefs.GetInt("level").ToString();

        }
        Degerim = b;

    }

    void Update()
    {
        if (i<b)
        {
            Zombie = Instantiate(prefabZombie);
            Zombie.transform.parent = this.transform;
            Zombie.GetComponent<Zombie>().Platform = this.gameObject;
            i += 1;
        }
        if (Degerim == 0)
        {
            GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().Bitti = true;
            col1.SetActive(false); //E�er aksi olmazsa bu iki sat�r� sil ve yerine thisli setactive yaz...
            col2.SetActive(false);
            Degerim = -1;
            if (finish)
            {
                karakter.GetComponent<SwipeDetector>().hiz = 0;
                winpanel.SetActive(true);
                PlayerPrefs.SetInt("level", PlayerPrefs.GetInt("level") + 1);
            }
            
        }
    }
}
