using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Duvar : MonoBehaviour
{
    public int GecmeDegeri;
    public int level, beklemesuresi;
    public GameObject explosion, karakter, TekParca, CokParca, WonPanel, DeadPanel, TextDegerim;
    public AudioSource aud;
    public Text LevelTxt;
    public bool Degdi;

    void Start()
    {
        level = PlayerPrefs.GetInt("Level");
        if (level == 0)
        {
            PlayerPrefs.SetInt("Level", 1);
            level = 1;
        }
        LevelTxt.text = level.ToString();
        GecmeDegeri = level + 9;
        beklemesuresi = 250;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        TextDegerim.GetComponent<TextMesh>().text = GecmeDegeri.ToString();
        if (Degdi)
        {
            beklemesuresi--;
            if(GecmeDegeri <= 0)
            {
                TekParca.SetActive(false);
                CokParca.SetActive(true);
                WonPanel.SetActive(true);
                PlayerPrefs.SetInt("Level", level + 1);
            }
            else
            {
                if(beklemesuresi <= 0)
                {
                    DeadPanel.SetActive(true);                }
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Fire")
        {
            Degdi = true;
            GecmeDegeri -= 1;
            GameObject.Instantiate(explosion, other.gameObject.transform.position, Quaternion.identity);
            karakter.GetComponent<AudioSource>().Play();
            other.gameObject.SetActive(false);
            
        }
    }
}
