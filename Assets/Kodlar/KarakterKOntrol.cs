using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KarakterKOntrol : MonoBehaviour
{
    public GameObject[] roketler;
    public GameObject prefabRoket, Roket, SonSecilen, DeadPanel, WelcomePanel, ExplosionFirst,Roketim, Cam1,Cam2, sonuncum, FinishText;
    public int degerim, aldigimDeger, Fark, BulamamaSayisi, MaxDeger, q, degdiint;
    public bool iyi, kotu,bulamadim, Yaklasti,Geldi, Bitti, degdim;
    public Text DegerTxt;
    public float hizim;
    public AudioSource aud;


    void Start()
    {
        hizim = this.GetComponent<SwipeDetector>().hiz;
        FinishText.GetComponent<TextMesh>().text = (PlayerPrefs.GetInt("level") + 40).ToString();
    }

    void FixedUpdate()
    {
        
        if (degdim)
        {
            degdiint--;
            if (degdiint == 0)
            {
                degdim = false;
            }
        }
        DegerTxt.text = degerim.ToString();
        if (Bitti)
        {
            this.GetComponent<SwipeDetector>().hiz = hizim;
            aud.Stop();
            Yaklasti = false;
            Geldi = false;
            Bitti = false;
        }
        if(degerim <= 0)
        {
            degerim = 0;
            this.GetComponent<SwipeDetector>().enabled = false;
            Roketim.gameObject.SetActive(false);
            aud.Stop();
            DeadPanel.SetActive(true);
        }
        if (degerim > MaxDeger)
        {
            MaxDeger = degerim;
        }
        if(degerim == 1)
        {
            sonuncum = GameObject.Find(GetComponentInChildren<NewBehaviourScript>().adim);
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Finish")
        {
            Cam2.transform.position = Cam1.transform.position;
            Cam1.SetActive(false);
            Cam2.SetActive(true);
        }
        if(other.gameObject.tag == "Respawn")
        {
            other.gameObject.GetComponent<Rastgele>().degdi = true;
        }
        if (other.gameObject.tag == "Bars")
        {
            if (!degdim)
            {
                Debug.Log("Degdi");
                if (other.gameObject.GetComponent<islem>().carp)
                {
                    aldigimDeger = degerim + other.gameObject.GetComponent<islem>().b;
                    Fark = aldigimDeger - degerim;
                    for (int i = 0; i < Fark; i++)
                    {
                        Roket = Instantiate(prefabRoket);
                        Roket.transform.parent = this.transform;
                        degerim += 1;
                        if (GameObject.Find("RPG-7 Rocket" + (degerim - 1).ToString()) == null)
                        {
                            Roket.name = "RPG-7 Rocket" + (degerim - 1).ToString();
                        }
                        else
                        {
                            Roket.name = "RPG-7 Rocket" + q.ToString();
                            q += 1;
                        }

                    }
                    Fark = 0;
                    iyi = true;
                    kotu = false;
                }
                if (other.gameObject.GetComponent<islem>().topla)
                {
                    aldigimDeger = degerim + other.gameObject.GetComponent<islem>().b;
                    Fark = aldigimDeger - degerim;
                    for (int i = 0; i < Fark; i++)
                    {
                        Roket = Instantiate(prefabRoket);
                        Roket.transform.parent = this.transform;
                        degerim += 1;
                        if (GameObject.Find("RPG-7 Rocket" + (degerim - 1).ToString()) == null)
                        {
                            Roket.name = "RPG-7 Rocket" + (degerim - 1).ToString();
                        }
                        else
                        {
                            Roket.name = "RPG-7 Rocket" + q.ToString();
                            q += 1;
                        }
                    }
                    Fark = 0;
                    iyi = true;
                    kotu = false;
                }
                if (other.gameObject.GetComponent<islem>().bol)
                {
                    aldigimDeger = degerim - other.gameObject.GetComponent<islem>().b;
                    Fark = degerim - aldigimDeger;

                    for (int i = 0; i < 1000; i++)
                    {
                        if (GameObject.Find("RPG-7 Rocket" + i.ToString()) != null)
                        {
                            SonSecilen = GameObject.Find("RPG-7 Rocket" + i.ToString());
                            GameObject.Instantiate(SonSecilen.GetComponent<NewBehaviourScript>().BoomPrefab, SonSecilen.transform.position, Quaternion.identity);
                            SonSecilen.SetActive(false);
                            SonSecilen.name = "YokEdildi";
                            degerim -= 1;
                            Fark -= 1;
                        }
                        else
                        {
                            if (degerim == 1)
                            {
                                degerim = 0;
                            }
                        }
                        if (Fark == 0)
                        {
                            i = 1000;
                        }

                    }
                    kotu = true;
                    iyi = false;
                    this.GetComponent<AudioSource>().Play();

                }
                if (other.gameObject.GetComponent<islem>().cikar)
                {
                    aldigimDeger = degerim - other.gameObject.GetComponent<islem>().b;
                    Fark = degerim - aldigimDeger;

                    for (int i = 0; i < 1000; i++)
                    {
                        if (GameObject.Find("RPG-7 Rocket" + i.ToString()) != null)
                        {
                            SonSecilen = GameObject.Find("RPG-7 Rocket" + i.ToString());
                            GameObject.Instantiate(SonSecilen.GetComponent<NewBehaviourScript>().BoomPrefab, SonSecilen.transform.position, Quaternion.identity);
                            SonSecilen.SetActive(false);
                            SonSecilen.name = "YokEdildi";
                            degerim -= 1;
                            Fark -= 1;
                        }
                        else
                        {
                            if (degerim == 1)
                            {
                                degerim = 0;
                            }
                        }
                        if (Fark == 0)
                        {
                            i = 1000;
                        }

                    }
                    kotu = true;
                    iyi = false;
                    this.GetComponent<AudioSource>().Play();

                }
            }
            degdim = true;
            degdiint = 60;
        }
        if (other.gameObject.tag == "ZombieYaklasma")
        {

            other.GetComponent<CollideAktif>().platform.GetComponent<ZombieYarat>().degildi = true;
            Yaklasti = true;
            this.GetComponent<SwipeDetector>().hiz = hizim / 2;
            other.gameObject.GetComponent<BoxCollider>().isTrigger = true;
           // Roketim.GetComponent<BoxCollider>().isTrigger = false;
        }
        if (other.gameObject.tag == "ZombieGeldi")
        {
            this.GetComponent<SwipeDetector>().hiz = 1;
            aud.Play();
            Geldi = true; 
            other.gameObject.GetComponent<BoxCollider>().isTrigger = true;
        }
    }

    public void Restart()
    {
        Application.LoadLevel(0);
    }

    public void startGame()
    {
        WelcomePanel.SetActive(false);
       // ExplosionFirst.SetActive(true);
        this.GetComponent<SwipeDetector>().enabled = true;
    }

}
