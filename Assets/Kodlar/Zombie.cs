using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    public GameObject Platform;
    public float  x,z, hiz;

    void Start()
    {
        hiz = 1.2f;
        x = Random.Range(-0.448f,0.434f);
        z = Random.Range(-0.393f,0.418f);
        this.transform.localPosition = new Vector3(x, 1, z);
        this.transform.localEulerAngles = new Vector3(0, -90, 0);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Platform.GetComponent<ZombieYarat>().degildi)
        {
            if (GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().degerim > 1)
            {
                transform.LookAt(GameObject.Find("Karakter").transform.position);
                transform.Translate(0, 0, hiz);
                this.GetComponent<Animator>().Play("Run");
            }
            else if (GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().degerim == 1)
            {
                transform.LookAt(GameObject.Find("Karakter").GetComponent<KarakterKOntrol>().sonuncum.transform.position);
                transform.Translate(0, 0, hiz);
                this.GetComponent<Animator>().Play("Run");
            }
            else
            {
                this.GetComponent<Animator>().Play("idle");
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            GameObject.Instantiate(other.gameObject.GetComponent<NewBehaviourScript>().BoomPrefab, this.transform.localPosition, Quaternion.identity);
            Platform.GetComponent<ZombieYarat>().Degerim -= 1;
            other.GetComponent<NewBehaviourScript>().Olum(this.GetComponent<BoxCollider>());
            this.gameObject.SetActive(false);
        }
    }
}
