using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public Material[] mats;
    public int a, level;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        level = PlayerPrefs.GetInt("level");
        a = level % 5;
        RenderSettings.skybox = mats[a];
    }
}
